<?php

namespace App\Helpers;

trait JsonResponses {

    /**
     * @param string $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function ok(string $message, array $data)
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data['data']
        ], 200);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function badRequest(string $message)
    {
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => []
        ], 422);
    }

}
