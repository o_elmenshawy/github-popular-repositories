<?php

namespace App\Http\Services;

use App\Http\Resources\TopStarsResource;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class PopularRepositoriesService {

    /**
     * @var \GuzzleHttp\Client
     */
    public $client;

    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * Get github top stars
     */
    public function getTopStars(array $data)
    {
        $response = $this->client->request('GET', config('app.github_top_stars_url'), ['query' => [
            'q' => "created:>{$data['date']}" . (isset($data['language'])?" language:{$data['language']}":""),
            'sort' => 'stars',
            'order' => 'desc',
            'per_page' => isset($data['per_page']) ? (int) $data['per_page'] : config('app.default_per_page')
        ]]);
        $response_body = json_decode($response->getBody(), true);
        $repositories = $response_body['items'];
        $resource = new Collection($repositories, new TopStarsResource);
        $data = (new Manager())->createData($resource)->toArray();
        return $data;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validate(array $data) :array
    {
        // validate request data
        foreach($data as $key => $value) {
            if (in_array($key, ['date', 'language', 'per_page'])) {
                $data[$key] = htmlspecialchars(stripslashes(trim($value)));
            } else {
                unset($data[$key]);
            }
        }

        // validate date parameter
        if (!isset($data['date'])) {
            $error = 'date is required.';
        } else if (! preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $data['date'])) {
            $error = 'date format is incorrect.';
        }

        // validate per_page parameter
        else if (isset($data['per_page']) && ($data['per_page'] < 1 || $data['per_page'] > config('app.max_per_page'))) {
            $error = 'per_page must be a positive number between 1 and 100';
        }

        return [
            'data' => $data,
            'error' => $error ?? null
        ];
    }
}
