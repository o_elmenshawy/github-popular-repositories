<?php

namespace App\Http\Resources;


use League\Fractal;

class TopStarsResource extends Fractal\TransformerAbstract
{
    public function transform($repository)
    {
        return [
            'id' => (int) $repository['id'],
            'repository_url' => $repository['html_url'],
            'clone_url' => $repository['git_url'],
            'name' => $repository['name'],
            'full_name' => $repository['full_name'],
            'owner_name' => $repository['owner']['login'],
            'is_private' => (bool) $repository['private']
        ];
    }
}
