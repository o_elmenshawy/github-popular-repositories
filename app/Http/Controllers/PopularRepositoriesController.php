<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helpers\JsonResponses;
use App\Http\Services\PopularRepositoriesService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PopularRepositoriesController extends Controller
{
    use JsonResponses;

    /**
     * @var PopularRepositoriesService
     */
    public $service;

    /**
     * @param PopularRepositoriesService $service
     */
    public function __construct(PopularRepositoriesService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request) : JsonResponse
    {
        $validation = $this->service->validate($request->all());
        if ($validation['error']) {
            return $this->badRequest($validation['error']);
        }
        $data = $this->service->getTopStars($validation['data']);
        return $this->ok(__('Top stars list displayed successfully.'), $data);
    }
}
