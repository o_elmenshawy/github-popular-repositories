# Github popular repositories

This is a solution to get Github popular repositories using Laravel framework

## Installation

- Install composer:

```bash
composer install
```
then copy `.env.example` to `.env`


- Build docker containers:

```bash
./vendor/bin/sail up -d
```

Now you can listen at `http://localhost`


## API usage

```bash
GET http://localhost/api/github-popular-repositories
```
Query parameters:

- `date` [required]: date with `yyyy-mm-dd` format

- `language` [optional]:  string

- `per_page` [optional]:  integer from `1` to `100`, default value is `10`

Example:

```bash
GET http://localhost/api/github-popular-repositories?date=2019-10-01&language=php&per_page=10
```


## Testing

You can run the tests with:
```bash
./vendor/bin/sail test
```
