<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;

use Tests\TestCase;

class GetGithubPopularRepositoriesTest extends TestCase
{
//    /**
//     * A basic test example.
//     *
//     * @return void
//     */
//    public function test_example()
//    {
//        $this->assertTrue(true);
//    }

    const URL = '/api/github-popular-repositories';

    /**
     * @var \GuzzleHttp\Client
     */
    public $client;

    public function __construct()
    {
        parent::__construct();
        $this->client = new \GuzzleHttp\Client();
    }

    public function testGetGithubTopStarsWithValidData()
    {
        $response = $this->client->request('GET',config('app.url') . self::URL, ['query' => [
            'date' => '2019-01-10',
            'language' => 'php',
            'per_page' => 10
        ]]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(10, json_decode($response->getBody(), true)['data']);
    }

    public function testGetGithubTopStarsWithoutDate()
    {
        $response = $this->client->request('GET', config('app.url') . self::URL, ['http_errors' => false, 'query' => [
            'language' => 'php',
            'per_page' => 10
        ]]);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertStringContainsString("date is required.", $response->getBody());
    }

    public function testGetGithubTopStarsWithIncorrectDateFormat()
    {
        $response = $this->client->request('GET', config('app.url') . self::URL, ['http_errors' => false, 'query' => [
            'date' => '2019/01/10',
            'language' => 'php',
            'per_page' => 10
        ]]);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertStringContainsString("date format is incorrect.", $response->getBody());
    }
}
